const shell = require('shelljs')

async function main () {
  try {
    shell.exec('yarpm run pm2 stop all')
    shell.exec('yarpm run pm2 kill')
  } catch (e) {
    console.log(e)
  }
}
main()
