# d-todo-list

A decentralized todo list app built on top of shardus to demostrate shardus

## Starting the network
When starting a shardus network we have two option 
- using `npm run start`
- using shardus cli

<Callout emoji="[x]" type="error">

only use one method at a time to start the network

</Callout>

### Using `npm run start`
This will start a network consist of only one node. 

To start a network with this method do -
```bash
npm run start
```
To stop the network - 
```bash
npm run stop
```
To clean the residual files and folders left by `npm run start`. please do -
```bash
npm run clean
```
To restart the network -
```bash
npm run stop && npm run clean && npm run start
```

### Using shardus cli to start the network
This method allows use to start a network consisting as many node as we want.
Since shardus cli read `package.json` to find `index.js` to start the nodes and our codes is in typescript, let's compile our code

To compile 
```bash
npm run compile
```
To start the network consisting 20 nodes.
```bash
shardus create-net 20
```

To stop the network -
```bash
shardus stop-net 
```

To clean residual folders and files, please do - 
```bash
shardus clean-net
```

## Interact with the network
To interact with the network let's start the client by doing `npm run client`. This will prompt you like this
```bash
~input:
```
Try typing `help`
```bash
~input: help

  Commands:

    help [command...]              Provides help for a given command.
    exit                           Exits application.
    todo add <list> <username>     Add todo to your list associated with your username, Example `todo add "play game and have dinner" joe`
    todo remove <list> <username>  Remove todo from your list associated with your username, Example `todo remove "play game and have dinner" joe`
    todo view <username>           View a list of todo for a specific user
    state                          Query the database
```
#### Adding todo list
```bash
~input: todo add "code and have dinner and sleep well" john
```
#### Viewing the list
To view the todo list of user john
```bash
~input: todo view john
```
Output will be:
```bash
1.code
2.have dinner
3.sleep well
```
#### Let's see what accounts database look like
To see all accounts do:
```bash
~input: state
```
Output:
```bash
{
  'b682e5326e0934c693c9ca334f69741c7492121592c04b9b76a6c28a2bbcc48a': {
    id: 'b682e5326e0934c693c9ca334f69741c7492121592c04b9b76a6c28a2bbcc48a',
    username: 'john',
    todo: [ 'code', 'have dinner', 'sleep well' ],
    timestamp: 1647083258912
  }
}
```
