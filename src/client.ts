const vorpal = require('vorpal')();
import * as got from 'got';
import * as crypto from '@shardus/crypto-utils';

crypto.init('69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc');

const BASEURL = 'http://localhost:9001';

async function inject(url, obj): Promise<string> {
  const response = await got.post(url, {
    method: 'POST',
    headers: {'content-type': 'application/json'},
    body: JSON.stringify(obj),
  });
  return response.body;
}

vorpal
  .command(
    'todo add <list> <username>',
    'Add todo to your list associated with your username, Example `todo add "play game and have dinner" joe`'
  )
  .action(async (args, cb) => {
    const todo = args.list.split('and').map(el => {
      return el.trim();
    });

    const tx = {
      type: 'add_todo',
      accountId: crypto.hash(args.username),
      username: args.username,
      timestamp: Date.now(),
      todo: todo,
    };

    const API = BASEURL + '/inject';
    const res = await inject(API, tx);

    vorpal.log(JSON.parse(res));

    cb();
  });

vorpal
  .command(
    'todo remove <list> <username>',
    'Remove todo from your list associated with your username, Example `todo remove "play game and have dinner" joe`'
  )
  .action(async (args, cb) => {
    const todo = args.list.split('and').map(el => {
      return el.trim();
    });

    const tx = {
      type: 'remove_todo',
      accountId: crypto.hash(args.username),
      username: args.username,
      timestamp: Date.now(),
      todo: todo,
    };
    const API = BASEURL + '/inject';
    const res = await inject(API, tx);
    vorpal.log(JSON.parse(res));

    cb();
  });

vorpal
  .command('todo view <username>', 'View a list of todo for a specific user')
  .action(async (args, cb) => {
    const API = BASEURL + `/list/${crypto.hash(args.username)}`;
    const res = await got.get(API);
    const list = JSON.parse(res.body).todo;

    list.map((el, i) => {
      console.log(`${i + 1}.${el}`);
    });
    cb();
  });

vorpal.command('state', 'Query the database').action(async (args, cb) => {
  const API = BASEURL + '/accounts';
  const res = await got.get(API);
  vorpal.log(JSON.parse(res.body));
  cb();
});

vorpal.delimiter('~input:').show();
