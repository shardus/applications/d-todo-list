import {shardusFactory} from '@shardus/core';
import * as crypto from '@shardus/crypto-utils';

crypto.init('69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc');

const config = {
  server: {
    p2p: {
      minNodesToAllowTxs: 1,
    },
  },
};

const dapp = shardusFactory(config);


type Account = {
  username: string;
  todo: string[];
  id: string;
  timestamp: number;
};

type Accounts = {
  [accountId: string]: Account;
};

type Transaction = {
  type: string;
  accountId?: string;
  username?: string;
  todo?: string[];
  timestamp: number;
};

type WrappedAccount = {
  data: Account;
  timestamp: number;
  stateId: string;
  accountId: string;
};

type WrappedAccounts = {
  [accountId: string]: WrappedAccount;
};

let accounts: Accounts = {}; // this is our database

dapp.registerExternalPost('inject', (req, res) => {
  try {
    const response = dapp.put(req.body);
    res.json(response);
  } catch (e) {
    res.json(e);
  }
});
dapp.registerExternalGet('list/:id', (req, res) => {
  try {
    const id = req.params['id'];
    res.json(accounts[id]);
  } catch (e) {
    res.json(e);
  }
});
dapp.registerExternalGet('accounts', (req, res) => {
  try {
    res.json(accounts);
  } catch (e) {
    res.json(e);
  }
});
dapp.setup({
  validate(tx: Transaction) {
    console.log('==> validate');
    if (
      tx.accountId === undefined ||
      typeof tx.timestamp !== 'number' ||
      tx.type === undefined
    ) {
      return {
        success: false,
        reason: 'Critical Attributes missing',
      };
    }

    switch (tx.type) {
      case 'remove_todo': {
        if (!Array.isArray(tx.todo)) {
          return {
            success: false,
            reason: 'Todo list must be an array',
          };
        }
        return {
          success: true,
          reason: '',
        };
      }
      case 'add_todo': {
        if (!Array.isArray(tx.todo)) {
          return {
            success: false,
            reason: 'Todo list must be an array',
          };
        }
        return {
          success: true,
          reason: '',
        };
      }
    }
  },
  apply(tx: Transaction, wrappedAccounts: WrappedAccounts) {
    console.log('==> apply');
    switch (tx.type) {
      case 'add_todo': {
        const oldList = wrappedAccounts[tx.accountId].data.todo;
        wrappedAccounts[tx.accountId].data.todo = [...oldList, ...tx.todo];
        break;
      }
      case 'remove_todo': {
        const oldList = wrappedAccounts[tx.accountId].data.todo;

        // remove the lists
        tx.todo.map(el => {
          wrappedAccounts[tx.accountId].data.todo = oldList.filter(
            todo => todo === el
          );
        });
        break;
      }
    }
    return dapp.createApplyResponse(crypto.hashObj(tx), tx.timestamp);
  },
  crack(tx: Transaction) {
    console.log('==> crack');
    return {
      id: crypto.hashObj(tx),
      timestamp: tx.timestamp,
      keys: {
        sourceKeys: [tx.accountId],
        targetKeys: [tx.accountId],
        allKeys: [tx.accountId],
        timestamp: tx.timestamp,
      },
    };
  },
  setAccountData(accountsToSet: Account[]) {
    console.log('==> setAccountData');
    accountsToSet.forEach(account => (accounts[account.id] = account));
  },
  resetAccountData(accountBackupCopies) {
    for (const recordData of accountBackupCopies) {
      const accountData = recordData.data
      accounts[accountData.id] = {...accountData}
    }
  },
  deleteAccountData(addressList: string[]) {
    console.log('==> deleteAccountData');
    addressList.forEach(address => delete accounts[address]);
  },
  deleteLocalAccountData() {
    console.log('==> deleteLocalAccountData');
    accounts = {};
  },
  getRelevantData(accountId, tx: Transaction) {
    console.log('==> getRelevantData');
    let account: Account = accounts[accountId];
    let accountCreated = false;

    if (!account) {
      account = {
        id: accountId,
        username: tx.username,
        todo: [],
        timestamp: tx.timestamp,
      };
      accountCreated = true;
    }
    return dapp.createWrappedResponse(
      accountId,
      accountCreated,
      crypto.hashObj(account),
      account.timestamp,
      account
    );
  },
  getAccountData(accountIdStart, accountIdEnd, maxRecords) {
    console.log('==> getAccountData');
    const wrappedAccounts: WrappedAccount[] = [];
    const start = parseInt(accountIdStart, 16);
    const end = parseInt(accountIdEnd, 16);

    for (const account of Object.values(accounts)) {
      const parsedAccountId = parseInt(account.id, 16);
      if (parsedAccountId < start || parsedAccountId > end) continue;

      const wacc = dapp.createWrappedResponse(
        account.id,
        false,
        crypto.hashObj(account),
        account.timestamp,
        account
      );

      wrappedAccounts.push(wacc);

      if (wrappedAccounts.length >= maxRecords) return wrappedAccounts;
    }
    return wrappedAccounts;
  },
  getAccountDataByRange(
    accountStart: string,
    accountEnd: string,
    dateStart: number,
    dateEnd: number,
    maxRecords: number,
  ) {
    console.log('==> getAccountDataByRange');
    const wrappedAccounts: WrappedAccount[] = [];

    const start = parseInt(accountStart, 16);
    const end = parseInt(accountEnd, 16);

    for (const account of Object.values(accounts)) {
      // Skip if not in account id range
      const id = parseInt(account.id, 16);
      if (id < start || id > end) continue;

      // Skip if not in timestamp range
      const timestamp = account.timestamp;
      if (timestamp < dateStart || timestamp > dateEnd) continue;

      const wrappedAccount = dapp.createWrappedResponse(
        account.id,
        false,
        crypto.hashObj(account),
        account.timestamp,
        account
      );

      wrappedAccounts.push(wrappedAccount);

      // Return results early if maxRecords reached
      if (wrappedAccounts.length >= maxRecords) return wrappedAccounts;
    }

    return wrappedAccounts;
  },
  getAccountDataByList(addressList: string[]) {
    console.log('==> getAccountDataByList');
    const wrappedAccounts: WrappedAccount[] = [];

    for (const address of addressList) {
      const account = accounts[address];

      if (!account) continue;

      const wacc = dapp.createWrappedResponse(
        account.id,
        false,
        crypto.hashObj(account),
        account.timestamp,
        account
      );
      wrappedAccounts.push(wacc);
    }
  },
  updateAccountFull(wrappedState, localCache: Account, applyResponse) {
    console.log('==> updateAccountFull');
    const {accountId, accountCreated} = wrappedState;
    const updatedAccount = wrappedState.data as Account;

    const hashBefore = accounts[accountId]
      ? crypto.hashObj(accounts[accountId])
      : '';

    const hashAfter = crypto.hashObj(updatedAccount);

    accounts[accountId] = updatedAccount;

    dapp.applyResponseAddState(
      applyResponse,
      updatedAccount,
      localCache,
      accountId,
      applyResponse.txId,
      applyResponse.txTimestamp,
      hashBefore,
      hashAfter,
      accountCreated
    );
  },
  updateAccountPartial(wrappedState, localCache: Account, applyResponse) {
    console.log('==> updateAccountPartial');
    this.updateAccountFull(wrappedState, localCache, applyResponse);
  },
  calculateAccountHash(account: Account) {
    console.log('==> calculateAccountHash');
    return crypto.hashObj(account);
  },
  close() {
    console.log('Shutting down...');
  },
});

dapp.registerExceptionHandler();
dapp.start();
